﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineObjectiveExamination.Core.Interfaces;
using OnlineObjectiveExamination.Core.Models;

namespace OnlineObjectiveExamination.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<T> : ControllerBase where T : DataEntity
    {
        private readonly IGenericRepository<T> _genericRepository;

        public GenericController(IGenericRepository<T> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        [HttpPost("Add")]
        public ActionResult<T> AddEntity([FromBody] T entityToAdd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = _genericRepository.AddEntity(entityToAdd);
                Save();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("AddAsync")]
        public async Task<ActionResult<T>> AddEntityAsync(T entityToAdd)
        {
            try
            {
                var result = await _genericRepository.AddEntityAsync(entityToAdd);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("AddList")]
        public ActionResult AddEntityList(List<T> listOfEntity)
        {
            try
            {
                _genericRepository.AddEntityList(listOfEntity);
                Save();
                return Ok("Successfully inserted.");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete("Delete/{guid}")]
        public ActionResult DeleteEntity(Guid guid)
        {
            try
            {
                var resultById = _genericRepository.GetEntityById(guid);
                _genericRepository.DeleteEntity(resultById);
                return Ok("Successfully deleted.");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete("DeleteAsync")]
        public async Task<ActionResult<int>> DeleteEntityAsync([FromBody]T entityToDelete)
        {
            try
            {
                var result = await _genericRepository.DeleteEntityAsync(entityToDelete);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> predicate)
        {
            return Ok(_genericRepository.FindByCondition(predicate));
        }

        public ActionResult<Task<IEnumerable<T>>> FindByConditionAsync(Expression<Func<T, bool>> predicate)
        {
            return _genericRepository.FindByConditionAsync(predicate);
        }

        [HttpGet("GetAll")]
        public ActionResult<IEnumerable<T>> GetAll()
        {
            try
            {
                var result = _genericRepository.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("GetAllAsync")]
        public async Task<ActionResult<IEnumerable<T>>> GetAllAsync()
        {
            try
            {
                var result = await _genericRepository.GetAllAsync();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("GetById/{guid}")]
        public ActionResult<T> GetEntityById(Guid guid)
        {
            try
            {
                var result = _genericRepository.GetEntityById(guid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("GetByIdAsync/{guid}")]
        public async Task<ActionResult<T>> GetEntityByIdAsync(Guid guid)
        {
            try
            {
                var result = await _genericRepository.GetEntityByIdAsync(guid);
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public T UpdateEntity(T entityToUpdate)
        {
            var result = _genericRepository.UpdateEntity(entityToUpdate);
            return result;
        }

        public ActionResult<T> AddEntityWithNoSave(T entityToAdd)
        {
            _genericRepository.AddEntityWithNoSave(entityToAdd);
            return Ok(entityToAdd);
        }

        public ActionResult AddEntityListWithNoSave(List<T> entityList)
        {
            _genericRepository.AddEntityListWithNoSave(entityList);
            return Ok("Successfully inserted.");
        }

        public void Save()
        {
            _genericRepository.Save();
        }
    }
}