﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineObjectiveExamination.Core.Models
{
    public class User : DataEntity
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public virtual IEnumerable<Question> Questions { get; set; }
    }
}