﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineObjectiveExamination.Core.Models
{
    public class Question : DataEntity
    {
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public string QuestionText { get; set; }
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public string Option4 { get; set; }
        public string AnswerText { get; set; }
        public string UserAnswer { get; set; }
    }
}