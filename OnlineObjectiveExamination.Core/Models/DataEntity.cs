﻿using System;

namespace OnlineObjectiveExamination.Core.Models
{
    public abstract class DataEntity
    {
        public Guid Id { get; set; }
    }
}