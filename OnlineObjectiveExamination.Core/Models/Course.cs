﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineObjectiveExamination.Core.Models
{
    public class Course : DataEntity
    {
        public string CourseName { get; set; }
        public int Status { get; set; }
        public virtual IEnumerable<Question> Questions { get; set; }
    }
}