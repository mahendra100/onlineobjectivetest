﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using OnlineObjectiveExamination.Core.Models;

namespace OnlineObjectiveExamination.Core.Interfaces
{
    /// <summary Author='Mahendra Pandey' Date='11 Sept 2018'>
    /// Base Methods for implementing GenericRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericRepository<T> where T : DataEntity
    {
        T AddEntity(T entityToAdd);

        Task<T> AddEntityAsync(T entityToAdd);

        T GetEntityById(Guid guid);

        Task<T> GetEntityByIdAsync(Guid guid);

        IEnumerable<T> GetAll();

        Task<IEnumerable<T>> GetAllAsync();

        void DeleteEntity(T entityToDelete);

        Task<int> DeleteEntityAsync(T entityToDelete);

        T UpdateEntity(T entityToUpdate);

        IEnumerable<T> FindByCondition(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> predicate);

        void AddEntityList(IEnumerable<T> listOfEntity);

        T AddEntityWithNoSave(T entityToAdd);

        void AddEntityListWithNoSave(List<T> entityList);

        void UpdateEntityListWithNoSave(List<T> entitiesList);

        void DeleteEntityListWithNoSave(List<T> entitiesList);

        void Save();
    }
}