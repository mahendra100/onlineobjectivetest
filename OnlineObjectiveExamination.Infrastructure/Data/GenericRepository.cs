﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OnlineObjectiveExamination.Core.Interfaces;
using OnlineObjectiveExamination.Core.Models;

namespace OnlineObjectiveExamination.Infrastructure.Data
{
    /// <summary Author='Mahendra Pandey' Date='11 Sept 2018'>
    /// Implementaion for GenericRepository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    //public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : DataEntity
    //{
    //    private readonly EstimateToolContext _context;
    //    private readonly DbSet<TEntity> _dbSet;

    //    public GenericRepository(EstimateToolContext context)
    //    {
    //        _context = context ?? throw new ArgumentNullException("Context was not supplied.");
    //        _dbSet = _context.Set<TEntity>();
    //    }

    //    public TEntity AddEntity(TEntity entityToAdd)
    //    {
    //        _dbSet.Add(entityToAdd);
    //        Save();
    //        return entityToAdd;
    //    }

    //    public void DeleteEntity(TEntity entityToDelete)
    //    {
    //        _dbSet.Attach(entityToDelete);
    //        _dbSet.Remove(entityToDelete);
    //        Save();
    //    }

    //    public TEntity UpdateEntity(TEntity entityToUpdate)
    //    {
    //        _dbSet.Attach(entityToUpdate);
    //        _context.Entry(entityToUpdate).State = EntityState.Modified;
    //        Save();
    //        return entityToUpdate;
    //    }

    //    public IEnumerable<TEntity> GetAll()
    //    {
    //        return _dbSet.AsEnumerable();
    //    }

    //    public IEnumerable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> predicate)
    //    {
    //        return _dbSet.Where(predicate);
    //    }

    //    public TEntity GetEntityById(Guid guid) => _dbSet.Find(guid);

    //    public void AddEntityList(IEnumerable<TEntity> listOfEntity)
    //    {
    //        _dbSet.AddRange(listOfEntity);
    //        Save();
    //    }

    //    public TEntity AddEntityWithNoSave(TEntity entityToAdd)
    //    {
    //        _dbSet.Add(entityToAdd);
    //        return entityToAdd;
    //    }

    //    public void AddEntityListWithNoSave(List<TEntity> entities)
    //    {
    //        _dbSet.AddRange(entities);
    //    }

    //    public void UpdateEntityListWithNoSave(List<TEntity> entities)
    //    {
    //        _dbSet.UpdateRange(entities);
    //    }

    //    public void DeleteEntityListWithNoSave(List<TEntity> entities)
    //    {
    //        _dbSet.AttachRange(entities);
    //        _dbSet.RemoveRange(entities);
    //    }

    //    #region AsyncMethods

    //    public async Task<TEntity> AddEntityAsync(TEntity entityToAdd)
    //    {
    //        await _dbSet.AddAsync(entityToAdd);
    //        await SaveAsync();
    //        return entityToAdd;
    //    }

    //    public async Task<int> DeleteEntityAsync(TEntity entityToDelete)
    //    {
    //        _dbSet.Remove(entityToDelete);
    //        return await SaveAsync();
    //    }

    //    public async Task<IEnumerable<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> predicate)
    //    {
    //        return await _dbSet.Where(predicate).ToListAsync();
    //    }

    //    public async Task<IEnumerable<TEntity>> GetAllAsync()
    //    {
    //        return await _dbSet.ToListAsync();
    //    }

    //    public async Task<TEntity> GetEntityByIdAsync(Guid guid)
    //    {
    //        return await _dbSet.FindAsync(guid);
    //    }

    //    #endregion AsyncMethods

    //    public void Save() => _context.SaveChanges();

    //    public async Task<int> SaveAsync() => await _context.SaveChangesAsync();
    //}
}