﻿using Microsoft.EntityFrameworkCore;
using OnlineObjectiveExamination.Core.Models;

namespace OnlineObjectiveExamination.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<User> User { get; set; }
    }
}